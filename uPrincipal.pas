unit uPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Layouts, System.Actions, FMX.ActnList, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls;

type
  TfrmPrincipal = class(TForm)
    lytBackground: TLayout;
    tbctrlPrincipal: TTabControl;
    tbitemMenu: TTabItem;
    tbitemApoio: TTabItem;
    lytPrincipal: TLayout;
    actAcoes: TActionList;
    actMudarAba: TChangeTabAction;
    lytSuperior: TGridLayout;
    lytInferior: TGridLayout;
    lytMenu: TGridLayout;
    lytPrimeiroBotao: TLayout;
    rnbPrimeiroBotao: TRoundRect;
    lytRotulosPrimeiroBotao: TLayout;
    lblTitulorimeiroBotao: TLabel;
    imgPrimeiroBotao: TImage;
    lblDescricaoPrimeiroBotao: TLabel;
    lytSegundoBotao: TLayout;
    rnbSegundoBotao: TRoundRect;
    lytRotulosSegundoBotao: TLayout;
    lblTituloSegundoBotao: TLabel;
    lblDescricaoSegundoBotao: TLabel;
    imgSegundoBotao: TImage;
    procedure FormCreate(Sender: TObject);
    procedure imgPrimeiroBotaoClick(Sender: TObject);
  private
    { Private declarations }
    FActiveForm : TForm;
  public
    { Public declarations }
    procedure MudarAba(ATabItem: TTabItem; Sender: TObject);
    procedure AbrirForm(AFormClass: TComponentClass);
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.fmx}

procedure TfrmPrincipal.AbrirForm(AFormClass: TComponentClass);
var
  LayoutBase, BotaoMenu: TComponent;
begin
  if Assigned(FActiveForm) then
  begin
    if FActiveForm.ClassType = AFormClass then
      exit
    else
    begin
      FActiveForm.DisposeOf; //N�o usar Free
      FActiveForm := nil;
    end;
  end;

  Application.CreateForm(AFormClass, FActiveForm);
  // encontra o Layoutbase no fomr a ser exibido para adiocionar ao frmPrincipal
  LayoutBase := FActiveForm.FindComponent('lytBase');
  if Assigned(LayoutBase) then
    lytPrincipal.AddObject(TLayout(LayoutBase));

  //encontra o bot�o de controle de menu no form a ser exibido para
  // associ�-lo ao MultView do frmPrincipal
//  BotaoMenu := FActiveForm.FindComponent('btnMenu');
//  if Assigned(BotaoMenu) then
//    mlvMenu.MasterButton := TControl(BotaoMenu);

end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  tbctrlPrincipal.ActiveTab := tbitemMenu;
  tbctrlPrincipal.TabPosition := TTabPosition.None;
end;

procedure TfrmPrincipal.imgPrimeiroBotaoClick(Sender: TObject);
begin
  AbrirForm(TfrmClientes);
  MudarAba(tbitemApoio, Sender);
end;

procedure TfrmPrincipal.MudarAba(ATabItem: TTabItem; Sender: TObject);
begin
  actMudarAba.Tab := ATabItem;
  actMudarAba.ExecuteTarget(Sender);
end;

end.
